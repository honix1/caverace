using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionQueue : MonoBehaviour
{
	private Stack<Action> actions = new Stack<Action>(32);

	public void EnqueueAction(Action action)
	{
		actions.Push(action);
	}

	void Update()
	{
		if (actions.Count > 0)
		{
			actions.Pop().Invoke();
		}
	}
}
