using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Profiling;

public class VoxelSurface : MonoBehaviour
{
	[SerializeField]
	private Volume volume;

	[SerializeField]
	private Material material;

	[SerializeField]
	private int volumeSize = 64;
	[SerializeField]
	private int chunkSide = 16;

	public void Genearate()
	{
		int volumeSizePlusOne = volumeSize + 1;
		volume.Initialize(volumeSizePlusOne);

		var watch = System.Diagnostics.Stopwatch.StartNew();

		JobHandle job = volume.GenerateVolume();

		watch.Stop();
		Debug.Log($"GenerateVolume {watch.ElapsedMilliseconds}");

		watch = System.Diagnostics.Stopwatch.StartNew();

		for (int x = 0; x < volumeSize; x += chunkSide)
			for (int y = 0; y < volumeSize; y += chunkSide)
				for (int z = 0; z < volumeSize; z += chunkSide)
				{
					var go = new GameObject("Surface");
					go.transform.SetParent(transform, worldPositionStays: false);

					var surface = go.AddComponent<Surface>();

					BoundsInt bound = new BoundsInt(
							x, y, z,
							chunkSide, 
							chunkSide, 
							chunkSide);

					surface.Initialize(volume, bound, material);
					surface.GenerateSurface(job);

					volume.volumeUpdate.AddListener(surface.VolumeUpdate);
				}

		watch.Stop();
		Debug.Log($"AddComponent<Surface> {watch.ElapsedMilliseconds}");
	}
}
