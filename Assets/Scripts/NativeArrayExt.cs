﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Collections;
using Unity.Jobs;

public static class NativeArrayExt
{
	public static T Get3D<T>(this NativeArray<T> array, int width, int x, int y, int z) where T : struct
	{
		return array[
			x * width * width +
			y * width +
			z
		];
	}

	public static async Task WhenJobDone(this JobHandle jobHandle, Action action)
	{
		while (!jobHandle.IsCompleted)
		{
			await Task.Delay(100);
		}
		jobHandle.Complete();

		action.Invoke();
	}
}
