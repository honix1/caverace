using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
	void OnFire()
	{
		//Debug.Log("FIRE");

		if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hitInfo))
		{
			//Debug.Log(hitInfo);

			if (hitInfo.collider.TryGetComponent<Surface>(out var surface))
			{
				surface.Punch(hitInfo.point, 25f);
			}
		}
	}
}
