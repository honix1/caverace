using System.Collections.Generic;
using System.Linq;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Events;
using static SimplexNoise.Noise;

public class Volume : MonoBehaviour
{
	public UnityEvent<BoundsInt> volumeUpdate;

	public int volumeWidth;

	private static NativeArray<Vector3Int> coords;
	public NativeArray<float> volume;

	private NativeArray<byte> perm;

	public void Initialize(int volumeWidth)
	{
		this.volumeWidth = volumeWidth;
		if (!coords.IsCreated)
		{
			coords = new NativeArray<Vector3Int>(Coords3D(volumeWidth, volumeWidth, volumeWidth).ToArray(), Allocator.Persistent);
		}
		volume = new NativeArray<float>(volumeWidth * volumeWidth * volumeWidth, Allocator.Persistent);
		perm = _perm; //new NativeArray<byte>(PermOriginal, Allocator.Persistent);
	}

	private IEnumerable<Vector3Int> Coords3D(int xSize, int ySize, int zSize)
	{
		for (int x = 0; x < xSize; x++)
			for (int y = 0; y < ySize; y++)
				for (int z = 0; z < zSize; z++)
					yield return new Vector3Int(x, y, z);
	}

	[BurstCompile]
	private struct VolumeJob : IJobParallelFor
	{
		[ReadOnly]
		public NativeArray<Vector3Int> coords;
		[WriteOnly]
		public NativeArray<float> volume;
		[ReadOnly]
		public NativeArray<byte> perm;

		public Vector3Int transformPosition;

		public void Execute(int i)
		{
			Vector3Int pos = coords[i];

			Vector3Int shiftPos = pos + transformPosition;

			//volume[i] =
			//	CalcPixel3D(shiftPos.x, shiftPos.y, shiftPos.z, 0.04f, perm) / 256 +
			//	CalcPixel3D(shiftPos.x, shiftPos.y, shiftPos.z, 0.1f, perm) / 256 * 0.01f;

			Vector3Int magPos = shiftPos + Vector3Int.one * 150;
			Vector3Int magPos2 = shiftPos + Vector3Int.one * 250;

			float h = shiftPos.y - 0.5f;

			float ground =
				h +
				CalcPixel3D(shiftPos.x, shiftPos.y, shiftPos.z, 0.2f, perm) / 256 *
					CalcPixel3D(magPos.x, magPos.y, magPos.z, 0.02f, perm) / 256 * 0.5f +
				math.pow(CalcPixel3D(magPos2.x, magPos2.y, magPos2.z, 0.01f, perm) / 256, 3) * 6f +
				math.pow(CalcPixel3D(magPos2.x, magPos2.y, magPos2.z, 0.002f, perm) / 256, 5) * 24f;

			volume[i] =
				math.min(
					math.max(
						ground,
						1 - CalcPixel3D(shiftPos.x, shiftPos.y * 2, shiftPos.z, 0.05f, perm) / 256 *
							math.clamp(math.pow(CalcPixel3D(magPos.x, magPos.y, magPos.z, 0.01f, perm), 1), 0, 256) / 256 * 5f),
						//math.min(
						//	1 - ground + 2.0f,
						math.max(0.25f, math.abs(ground)) * CalcPixel3D(shiftPos.x, shiftPos.y / 2, shiftPos.z, 0.03f, perm) / 256 *
								math.clamp(math.pow(CalcPixel3D(magPos.x, magPos.y, magPos.z, 0.0025f, perm), 1), 0, 256) / 256 * 5f);/*);*/
		}
	}

	public JobHandle GenerateVolume()
	{
		Vector3 transformPosition = transform.position;

		var job = new VolumeJob()
		{
			coords = coords,
			volume = volume,
			perm = perm,
			transformPosition = Vector3Int.FloorToInt(transformPosition)
		};

		return job.Schedule(coords.Length, 64);
	}

	public void Punch(Vector3 point, float radius)
	{
		point = point - Vector3Int.FloorToInt(transform.position);

		for (int i = 0; i < coords.Length; i++)
		{
			var pos = coords[i];
			float value = volume[i];
			volume[i] =
				Mathf.Max(
					value,
					1 - Vector3.Distance(
								point,
								pos) / radius);
		}

		volumeUpdate?.Invoke(
			new BoundsInt(
				new Vector3Int((int)point.x, (int)point.y, (int)point.z),
				Vector3Int.one * Mathf.CeilToInt(radius) * 2));
	}
}
