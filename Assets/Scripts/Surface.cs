using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

public class Surface : MonoBehaviour
{
	private Volume volume;

	private MeshFilter meshFilter;
	private MeshCollider meshCollider;

	private BoundsInt bounds;

	private Material material;

	public void Initialize(Volume volume, BoundsInt bounds, Material material)
	{
		this.volume = volume;
		this.bounds = bounds;
		this.material = material;
	}

	private static ActionQueue actionQueue;

	private void Awake()
	{
		if (actionQueue == null)
			actionQueue = FindObjectOfType<ActionQueue>();
	}

	internal void Punch(Vector3 point, float radius)
	{
		volume.Punch(point, radius);
	}

	public void VolumeUpdate(BoundsInt bounds)
	{
		Bounds ours = new Bounds(this.bounds.center, this.bounds.size);
		Bounds their = new Bounds(bounds.center, bounds.size);
		if (their.Intersects(ours))
		{
			GenerateSurface(default);
		}
	}

	public static IEnumerable<Vector3Int> Coords3D(int xFrom, int xTo, int yFrom, int yTo, int zFrom, int zTo)
	{
		for (int x = xFrom; x < xTo; x++)
			for (int y = yFrom; y < yTo; y++)
				for (int z = zFrom; z < zTo; z++)
					yield return new Vector3Int(x, y, z);
	}

	[BurstCompile]
	private struct GenerateCoords3D : IJob
	{
		public int xFrom, xTo, yFrom, yTo, zFrom, zTo;

		[WriteOnly]
		public NativeArray<Vector3Int> result;

		public void Execute()
		{
			int i = 0;
			for (int x = xFrom; x < xTo; x++)
				for (int y = yFrom; y < yTo; y++)
					for (int z = zFrom; z < zTo; z++)
					{
						result[i++] = new Vector3Int(x, y, z);
					}
		}
	}
	public static Vector3Int[] Coords3DFast(int xFrom, int xTo, int yFrom, int yTo, int zFrom, int zTo)
	{
		var result = new Vector3Int[(xTo - xFrom) * (yTo - yFrom) * (zTo - zFrom)];
		int i = 0;
		for (int x = xFrom; x < xTo; x++)
			for (int y = yFrom; y < yTo; y++)
				for (int z = zFrom; z < zTo; z++)
				{
					result[i++] = new Vector3Int(x, y, z);
				}
		return result;
	}

	[BurstCompile]
	private struct GenerateSurfaceJob : IJob
	{
		public BoundsInt bounds;
		public int volumeWidth;

		[ReadOnly]
		public NativeArray<Vector3Int> coords;
		[ReadOnly]
		public NativeArray<float> volume;
		[ReadOnly]
		public NativeArray<int> edgeTableNative;
		[ReadOnly]
		public NativeArray<int> triTableNative;

		[WriteOnly]
		public NativeArray<CubeMarching.Triangle> triangles;
		// Read write
		public NativeArray<int> ntriang; // singleton

		public void Execute()
		{
			foreach (Vector3Int pos in coords)
			{
				//var poss = pos + bounds.min;
				int x = pos.x;
				int y = pos.y;
				int z = pos.z;

				CubeMarching.GridCell gridCell = new CubeMarching.GridCell
				{
					point0 = new Vector3(x, y, z),
					point1 = new Vector3(x, y, z + 1),
					point2 = new Vector3(x + 1, y, z + 1),
					point3 = new Vector3(x + 1, y, z),
					point4 = new Vector3(x, y + 1, z),
					point5 = new Vector3(x, y + 1, z + 1),
					point6 = new Vector3(x + 1, y + 1, z + 1),
					point7 = new Vector3(x + 1, y + 1, z),

					value0 = volume.Get3D(volumeWidth, x, y, z),
					value1 = volume.Get3D(volumeWidth, x, y, z + 1),
					value2 = volume.Get3D(volumeWidth, x + 1, y, z + 1),
					value3 = volume.Get3D(volumeWidth, x + 1, y, z),
					value4 = volume.Get3D(volumeWidth, x, y + 1, z),
					value5 = volume.Get3D(volumeWidth, x, y + 1, z + 1),
					value6 = volume.Get3D(volumeWidth, x + 1, y + 1, z + 1),
					value7 = volume.Get3D(volumeWidth, x + 1, y + 1, z),
				};

				int newTriangles = CubeMarching.Polygonise(gridCell, 0.5f, ntriang[0], triangles, edgeTableNative, triTableNative);

				ntriang[0] += newTriangles;
			}
		}
	}


	public void GenerateSurface(JobHandle parentJob)
	{
		int xFrom = bounds.xMin, xTo = bounds.xMax,
			yFrom = bounds.yMin, yTo = bounds.yMax,
			zFrom = bounds.zMin, zTo = bounds.zMax;

		var job1 = new GenerateCoords3D()
		{
			result = new NativeArray<Vector3Int>((xTo - xFrom) * (yTo - yFrom) * (zTo - zFrom), Allocator.Persistent, NativeArrayOptions.UninitializedMemory),
			xFrom = xFrom,
			xTo = xTo,
			yFrom = yFrom,
			yTo = yTo,
			zFrom = zFrom,
			zTo = zTo,
		};

		var jobHandle1 = job1.Schedule();

		var job2 = new GenerateSurfaceJob()
		{
			ntriang = new NativeArray<int>(1, Allocator.Persistent),
			bounds = bounds,
			volumeWidth = volume.volumeWidth,
			coords = job1.result,
			volume = volume.volume,
			edgeTableNative = CubeMarching.edgeTableNative,
			triTableNative = CubeMarching.triTableNative,
			triangles = new NativeArray<CubeMarching.Triangle>(64 * 64 * 64, Allocator.Persistent, NativeArrayOptions.UninitializedMemory),
		};

		var jobHandle = job2.Schedule(JobHandle.CombineDependencies(parentJob, jobHandle1));

		StartCoroutine(Then(jobHandle, job1, job2));
	}

	private IEnumerator Then(JobHandle jobHandle, GenerateCoords3D job1, GenerateSurfaceJob job2)
	{
		while (!jobHandle.IsCompleted)
		{
			yield return null;
		}

		jobHandle.Complete();

		job1.result.Dispose();

		if (job2.ntriang[0] == 0)
		{
			job2.ntriang.Dispose();
			job2.triangles.Dispose();
		}
		else
		{
			int ntriang = job2.ntriang[0];
			int vertCount = ntriang * 3;
			Vector3[] vertices = new Vector3[vertCount];
			int[] tris = new int[vertCount];

			NativeArray<Vector3>.Copy(job2.triangles.Reinterpret<Vector3>(36), vertices, vertCount);

			int i = 0;
			while (i < ntriang)
			{
				tris[i * 3 + 0] = i * 3 + 2;
				tris[i * 3 + 1] = i * 3 + 1;
				tris[i * 3 + 2] = i * 3 + 0;

				i++;
			}

			job2.ntriang.Dispose();
			job2.triangles.Dispose();

			actionQueue.EnqueueAction(() =>
			{
				InitComponents();

				Mesh mesh = new Mesh();
				//mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32; // allow more than 65k vertices

				mesh.SetVertices(vertices, 0, vertCount);
				mesh.SetTriangles(tris, 0);

				mesh.RecalculateNormals();

				meshFilter.mesh = mesh;
				meshCollider.sharedMesh = mesh;
			});
		}
	}

	private bool componetsInitialized;

	private void InitComponents()
	{
		if (componetsInitialized == true)
			return;

		MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
		//meshRenderer.sharedMaterial = new Material(Shader.Find("Standard"));
		meshRenderer.sharedMaterial = material;

		meshFilter = gameObject.AddComponent<MeshFilter>();

		meshCollider = gameObject.AddComponent<MeshCollider>();

		componetsInitialized = true;
	}
}
