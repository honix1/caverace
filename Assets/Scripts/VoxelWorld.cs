using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;

public class VoxelWorld : MonoBehaviour
{
	[SerializeField]
	private VoxelSurface voxelChunkPrefab;
	[SerializeField]
	private Transform player;
	[SerializeField]
	private int chunkSize = 128;

	private Dictionary<Vector3Int, VoxelSurface> coordToVoxelChunk =
		new Dictionary<Vector3Int, VoxelSurface>();

	private Vector3Int[] genZone;

	// TODO: refactor + volume
	private IEnumerable<Vector3Int> Coords3D(int xSize, int ySize, int zSize)
	{
		for (int x = 0; x < xSize; x++)
			for (int y = 0; y < ySize; y++)
				for (int z = 0; z < zSize; z++)
					yield return new Vector3Int(x, y, z);
	}

	// TODO: refactor + surface
	private IEnumerable<Vector3Int> Coords3D(int xFrom, int xTo, int yFrom, int yTo, int zFrom, int zTo)
	{
		for (int x = xFrom; x <= xTo; x++)
			for (int y = yFrom; y <= yTo; y++)
				for (int z = zFrom; z <= zTo; z++)
					yield return new Vector3Int(x, y, z);
	}

	private void Start()
	{
		genZone = Coords3D(-1, 1, -1, 1, -1, 1).ToArray();
	}

	private static ActionQueue actionQueue;

	private void Update()
	{
		Vector3Int playerChunkCoord = Vector3Int.FloorToInt(player.position / chunkSize);

		if (false)
		{
			TryFreshChunk(playerChunkCoord);
		}
		else
		{
			if (actionQueue == null)
				actionQueue = FindObjectOfType<ActionQueue>();
			foreach (Vector3Int coordShift in genZone)
			{
				var coord = playerChunkCoord + coordShift;
				if ((!coordToVoxelChunk.ContainsKey(coord)))
				{
					actionQueue.EnqueueAction(() =>
					{
						TryFreshChunk(coord);

					});
				}
			}
		}
	}

	private bool TryFreshChunk(Vector3Int coord)
	{
		if (!coordToVoxelChunk.ContainsKey(coord))
		{
			VoxelSurface newChunk = Instantiate(voxelChunkPrefab);
			newChunk.transform.parent = transform;
			newChunk.transform.position = coord * chunkSize;

			coordToVoxelChunk.Add(coord, newChunk);

			newChunk.Genearate();

			return true;
		}

		return false;
	}
}
