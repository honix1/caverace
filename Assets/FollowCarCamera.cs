using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCarCamera : MonoBehaviour
{
	[SerializeField]
	private Rigidbody car;
	[SerializeField]
	private Transform slotPoint;

	public float followSpeed = 1f;

	private Vector3 cameraOffset;

	private void Start()
	{
		cameraOffset = slotPoint.position - car.position;
	}

	void Update()
	{
		Vector3 carVelocity = car.velocity;
		carVelocity.Scale(new Vector3(1, 0, 1));

		Vector3 cameraPos = new Vector3(
			slotPoint.position.x,
			car.position.y + cameraOffset.y,
			slotPoint.position.z);

		Vector3 localCameraPos = cameraPos - car.position;

		Vector3 p = car.position - cameraPos;
		p.Scale(new Vector3(1, 0, 1));

		float deg = Vector3.SignedAngle(carVelocity, p, Vector3.up);
		localCameraPos = Quaternion.AngleAxis(-deg, Vector3.up) * localCameraPos;

		Debug.DrawLine(car.position, car.position + localCameraPos, Color.red);
		Debug.DrawRay(car.position, carVelocity, Color.blue);
		Debug.Log(deg);

		transform.position = Vector3.Lerp(
			transform.position,
			car.position + localCameraPos,
			followSpeed * Time.deltaTime);

		transform.LookAt(car.position + carVelocity.normalized * 100 + Vector3.down * 50);
	}
}
